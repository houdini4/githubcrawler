#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
from modules.jsonparser import JSONReader, JSONWriter
from modules.crawler import GitHubCrawler


def main():
    assert len(sys.argv) == 3, "github_crawler.py only expects 2 argument"
    print("Starting Github crawler...")
    print("Reading input file: {}".format(sys.argv[1]))
    json_input = JSONReader(sys.argv[1]).read()
    print("JSON read!")

    gh_crawler = GitHubCrawler(json_input)
    json_webpages = gh_crawler.crawl()

    print("Writing output file: {}".format(sys.argv[2]))
    JSONWriter(json_webpages, sys.argv[2]).write()


if __name__ == '__main__':
    main()
