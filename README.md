## Github crawler

This is a Python 3 implementation of a Github crawler. This crawler search and returns all the links from the search result, but only process first page results.

This crawler receives a json_file with the following format:

```json
 {
  "keywords": [
    "openstack",
    "nova",
    "css"
  ],
  "proxies": [
    "194.126.37.94:8080",
    "13.78.125.167:8080"
  ],
  "type": "Repositories"
}
```
Where: 

**keywords** a list of keywords to be used as search terms.

**proxies** is a list of proxies, one of them should be selected and used randomly to perform all the HTTP request.

**type** is the the type of object we are searching for. Only are allowed 3 types: Repositories, Issues and Wikis.

And returns a json file with all urls of the results:

```json
[
  {
    "url": "https://github.com/atuldjadhav/DropBox-Cloud-Storage"
  }
]
```

### How to use it
```bash
python ./github_crawler.py $input_json_file $output_json_file
```
