import os
import unittest
from bs4 import BeautifulSoup
from modules.crawler import GitHubCrawler

THIS_DIR = os.path.dirname(os.path.abspath(__file__))


class GitHubCrawlerTest(unittest.TestCase):

    def setUp(self):
        self.correct_data_input = {
                      "keywords": [
                        "python",
                        "django-rest-framework",
                        "jwt"
                      ],
                      "proxies": [
                        "90.182.159.82:53803",
                        "41.169.159.58:53281",
                        "78.159.79.72:45597"
                      ],
                      "type": "Repositories"
                    }

        with open(os.path.join(THIS_DIR, 'data', 'page_content.txt' )) as f:
            page_content = f.read()

        self.soup = BeautifulSoup(page_content,'html.parser')
        self.gt_urls = ['https://github.com/GetBlimp/django-rest-framework-jwt', 'https://github.com/lock8/django-rest-framework-jwt-refresh-token', 'https://github.com/merixstudio/django-trench', 'https://github.com/pyaf/djangular', 'https://github.com/City-of-Helsinki/tunnistamo', 'https://github.com/sibtc/drf-jwt-example', 'https://github.com/chessbr/rest-jwt-permission', 'https://github.com/JayjeetAtGithub/Audition-Management-System', 'https://github.com/Firok/RestApp', 'https://github.com/antonioxtasis/Django2_API']

    def test_input_crawler(self):
        gt_keywords = ['python', 'django-rest-framework', 'jwt']
        gt_proxies = ['90.182.159.82:53803', '41.169.159.58:53281', '78.159.79.72:45597']
        gt_type = "Repositories"

        gh = GitHubCrawler(self.correct_data_input)
        self.assertEqual(gt_keywords, gh.keywords)
        self.assertEqual(gt_proxies, gh.proxies)
        self.assertEqual(gt_type, gh.type)

    def test_get_urls(self):
        result = GitHubCrawler.get_urls(self.soup)
        self.assertEqual(self.gt_urls, result)

    def test_get_owners(self):
        gt_owners = [{'owner': 'GetBlimp'}, {'owner': 'lock8'}, {'owner': 'merixstudio'}, {'owner': 'pyaf'}, {'owner': 'City-of-Helsinki'}, {'owner': 'sibtc'}, {'owner': 'chessbr'}, {'owner': 'JayjeetAtGithub'}, {'owner': 'Firok'}, {'owner': 'antonioxtasis'}]
        result = GitHubCrawler.get_owners(self.gt_urls)
        self.assertEqual(gt_owners, result)


if __name__ == '__main__':
    unittest.main()
