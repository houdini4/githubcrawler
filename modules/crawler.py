import re
import sys
import random
import requests
from bs4 import BeautifulSoup


class Web(object):

    def __init__(self, url, proxies):
        assert isinstance(url, str), "URL has to be a string"
        try:
            self._request = requests.get(url, proxies=proxies)
        except requests.exceptions.RequestException as e:  # This is the correct syntax
            print(e)
            sys.exit(1)

    @property
    def request(self):
        return self._request

    @property
    def status(self):
        return self.request.status_code

    @property
    def content(self):
        return self.request.text


class Crawler(object):
    def __init__(self):
        self._keyword_search = 'q='
        self._keyword_type = 'type='
        self._keyword_and = '&'

    @property
    def keyword_search(self):
        return self._keyword_search

    @property
    def keyword_type(self):
        return self._keyword_type

    @property
    def keyword_and(self):
        return self._keyword_and

    def crawl(self):
        raise NotImplementedError


class GitHubCrawler(Crawler):

    def __init__(self, data):
        Crawler.__init__(self)
        assert len(data.keys()) == 3, "Input JSON File has to be 3 keys"
        assert "keywords" in data.keys(), "'keywords' has to be as a key in input JSON File"
        assert "proxies" in data.keys(), "'proxies' has to be as a key in input JSON File"
        assert "type" in data.keys(), "'type' has to be as a key in input JSON File"

        valid_types = ["Repositories", "Wikis", "Issues"]
        assert data["type"] in valid_types, "Only Repositories, Wikis and Issues are valid types"

        self._data = data
        self._keywords = data['keywords']
        self._proxies = data['proxies']
        self._type = data['type']
        self._search_url = 'https://github.com/search?'

    @property
    def search_url(self):
        return self._search_url

    @property
    def keywords(self):
        return self._keywords

    @property
    def proxies(self):
        return self._proxies

    @property
    def type(self):
        return self._type

    @staticmethod
    def get_urls(soup, tag='a'):
        assert isinstance(soup, BeautifulSoup), "soup has to be BeatifulSoup object"
        all_a = soup.find_all(tag)
        urls = [x['href'] for x in all_a if 'data-hydro-click=' in str(x)]

        urls_complete = ["https://github.com" + url for url in urls]
        return urls_complete

    @staticmethod
    def get_owners(urls):
        owners = [re.findall("[\/\/].*[\/](.*)[\/]", url)[0] for url in urls]
        result_owners = []
        for owner in owners:
            o = dict()
            o['owner'] = owner
            result_owners.append(o)

        return result_owners

    @staticmethod
    def _convert_urls_to_json_format(urls, extra_info=False, info=None):
        assert extra_info & (info is not None), "You need to add info"
        assert extra_info & isinstance(info, list) & all([isinstance(x, dict) for x in info]), \
            "info has to be a list of dicts"

        output = []
        for url in urls:
            u = dict()
            u['url'] = url
            if extra_info:
                u['extra'] = dict()
                for i in info:
                    for k in i.keys():
                        u['extra'][k] = i[k]
                        output.append(u)

        return output

    def crawl(self, parser='html.parser', output_format='json'):
        print("Input values:")
        print("Keywords: {}".format(self.keywords))
        print("Type: {}".format(self.type))

        keywords = "+".join(self.keywords)
        url = self.search_url + self.keyword_search + keywords + self.keyword_and + self.keyword_type + self.type

        https_proxy = dict()
        https_proxy['https'] = random.choice(self.proxies)
        print("Proxy: {}".format(https_proxy['https']))

        print("Trying to connect...")
        webpage = Web(url, https_proxy)
        soup = BeautifulSoup(webpage.content, parser)
        print("Connection ready!")

        print("Getting URLS...")
        urls = self.get_urls(soup)
        print("Getting owners...")
        owners = self.get_owners(urls)

        if output_format == "json":
            urls = self._convert_urls_to_json_format(urls, extra_info=True, info=owners)

        return urls













