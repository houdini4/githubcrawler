import json


class JSONObject(object):

    def __init__(self, filename):
        assert isinstance(filename, str), "filename must be string"
        self._filename = filename

    @property
    def filename(self):
        return self._filename


class JSONReader(JSONObject):

    def __init__(self, filename):
        JSONObject.__init__(self, filename)

    def read(self):
        with open(self.filename) as data_json:
            data = json.load(data_json)

            return data


class JSONWriter(JSONObject):

    def __init__(self, data, filename):
        JSONObject.__init__(self, filename)
        assert isinstance(data, list), "data must be list"
        assert all([isinstance(x, dict) for x in data]), "data must be a list of dicts"
        self._data = data

    @property
    def data(self):
        return self._data

    def write(self, write_mode='w'):
        with open(self.filename, write_mode) as outfile:
            json.dump(self.data, outfile)


